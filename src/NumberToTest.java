import java.util.Scanner;

public class NumberToTest {
	
		int num1 = 5;
		int num2 = 10;
		
		public double add(double num1, double num2) {
			return num1 + num2;
		}

		public double sub(double num1, double num2) {
			return num1 - num2;
		}

		public double mul(double num1, double num2) {
			return num1 * num2;
		}

		public double div(double num1, double num2) {
			return num1 / num2;
		}


		public static void main(String[] args) {
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter the number 1 : ");
			double num1 = scan.nextDouble();

			System.out.print("Enter the number 2: ");
			double num2 = scan.nextDouble();

			System.out.println("Enter the Operataion \n"
					+ "A - Addition\n"
					+ "S - Substration\n" 
					+ "M - Multiplication\n"
					+ "D - Division");
			String op = scan.next();
			
			NumberToTest mo = new NumberToTest();
			switch (op) {
			case "A":
				System.out.println(num1+ "+" + num2);
				System.out.println(num1+ "+" + num2 + "=" + mo.add(num1, num2));
				break;
			case "S":
				System.out.println(num1+ "-" + num2);
				System.out.println(num1+ "-" + num2 + "=" + mo.sub(num1, num2));
				break;
			case "M":
				System.out.println(num1+ "*" + num2);
				System.out.println(num1+ "*" + num2 + "=" + mo.mul(num1, num2));
				break;
			case "D":
				System.out.println(num1+ "/" + num2);
				System.out.println(num1+ "/" + num2 + "=" + mo.div(num1, num2));
				break;
			default:
				System.out.println("Invalied operation !!!");
			}

			
		}
	
}
